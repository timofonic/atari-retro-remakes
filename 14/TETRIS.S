 TEXT 
Begin:
      MOVEA.L   A7,A5 
      LEA       L004C,A7
      MOVEA.L   4(A5),A5
      MOVE.L    #$35000,-(A7) 
      MOVE.L    A5,-(A7)
      MOVE.W    #0,-(A7)
      MOVE.W    #$4A,-(A7) 	;MSHRINK
      TRAP      #1
      ADDA.L    #$C,A7
      TST.L     D0
      BNE       L0001 
      BSR       L0003 
L0000:PEA       L0002(PC) 
      PEA       L0002(PC) 
      PEA       L003D 
      CLR.W     -(A7) 
      MOVE.W    #$4B,-(A7) 	;PEXEC
      TRAP      #1
      LEA       14(A7),A7 
L0001:CLR.W     -(A7) 	;PTERM0
      TRAP      #1
L0002:DCB.W     2,0 
L0003:BSR       L000A 
      BSR       L0004 
      PEA       L003B(PC) 
      MOVE.W    #1,-(A7)
      MOVE.W    #$19,-(A7) 	;IKBDWS 
      TRAP      #$E 
      ADDQ.L    #8,A7 
      PEA       L000E 
      MOVE.W    #$26,-(A7) 	;SUPEXEC
      TRAP      #$E 
      ADDQ.L    #6,A7 
      PEA       L000B(PC) 
      MOVE.W    #$26,-(A7) 	;SUPEXEC
      TRAP      #$E 
      ADDQ.L    #6,A7 
      BSR       L0025 
      PEA       L000F 
      MOVE.W    #$26,-(A7) 	;SUPEXEC
      TRAP      #$E 
      ADDQ.L    #6,A7 
      PEA       L003C(PC) 
      MOVE.W    #1,-(A7)
      MOVE.W    #$19,-(A7) 	;IKBDWS 
      TRAP      #$E 
      ADDQ.L    #8,A7 
      MOVE.L    L0046,-(A7) 
      MOVE.W    #$49,-(A7) 	;MFREE
      TRAP      #1
      ADDQ.L    #6,A7 
      MOVE.W    L0047,-(A7) 
      MOVE.L    L0048,-(A7) 
      MOVE.L    L0049,-(A7) 
      MOVE.W    #5,-(A7) 	;SETSCREEN
      TRAP      #$E 
      LEA       12(A7),A7 
      PEA       L004A(PC) 
      MOVE.W    #6,-(A7) 	;SETPALLETE 
      TRAP      #$E 
      ADDQ.L    #6,A7 
      RTS 
L0004:MOVE.W    #4,-(A7) 	;GETREZ 
      TRAP      #$E 
      ADDQ.L    #2,A7 
      CMP.W     #2,D0 
      BEQ       L0000 
      MOVE.W    D0,L0047
      PEA       L0005(PC) 
      MOVE.W    #$26,-(A7) 	;SUPEXEC
      TRAP      #$E 
      ADDQ.L    #6,A7 
      PEA       $81E8 
      MOVE.W    #$48,-(A7) 	;MALLOC 
      TRAP      #1
      ADDQ.L    #6,A7 
      TST.L     D0
      BEQ       L0001 
      MOVE.L    D0,L0046
      ADDI.L    #$100,D0
      CLR.B     D0
      MOVE.L    D0,L0045
      MOVE.W    #2,-(A7) 	;PHYSBASE 
      TRAP      #$E 
      ADDQ.L    #2,A7 
      MOVE.L    D0,L0048
      MOVE.W    #3,-(A7) 	;LOGBASE
      TRAP      #$E 
      ADDQ.L    #2,A7 
      MOVE.L    D0,L0049
      BRA       L0006 
L0005:LEA       L004B(PC),A0
      MOVEM.L   $FF8240.L,D0-D7 
      MOVEM.L   D0-D7,-(A0) 
      RTS 
L0006:CLR.W     -(A7) 
      PEA       $78000
      MOVE.L    L0045,-(A7) 
      MOVE.W    #5,-(A7) 	;SETSCREEN
      TRAP      #$E 
      LEA       12(A7),A7 
      PEA       L004B(PC) 
      MOVE.W    #6,-(A7) 	;SETPALLETE 
      TRAP      #$E 
      ADDQ.L    #6,A7 
      MOVE.W    #0,-(A7)
      PEA       L0040(PC) 
      MOVE.W    #$3D,-(A7) 	;FOPEN
      TRAP      #1
      ADDQ.L    #8,A7 
      TST.W     D0
      BMI       L0001 
      MOVE.W    D0,D6 
      PEA       $78000
      PEA       $80E8 
      MOVE.W    D6,-(A7)
      MOVE.W    #$3F,-(A7) 	;FREAD
      TRAP      #1
      LEA       12(A7),A7 
      MOVE.W    D6,-(A7)
      MOVE.W    #$3E,-(A7) 	;FCLOSE 
      TRAP      #1
      ADDQ.L    #4,A7 
      MOVE.W    #0,D6 
      LEA       L001D,A4
      MOVEQ     #0,D7 
      MOVEQ     #0,D5 
L0007:MOVE.W    D6,D7 
      LSL.W     #1,D7 
L0008:MOVE.W    D5,-(A7)
      MOVE.W    D6,-(A7)
      MOVE.W    #7,-(A7) 	;SETCOLOR 
      TRAP      #$E 
      ADDQ.L    #6,A7 
      CMP.W     0(A4,D7.W),D5 
      BEQ       L0009 
      ADDQ.W    #1,D5 
      BRA       L0008 
L0009:MOVEQ     #0,D5 
      ADDQ.W    #1,D6 
      CMP.W     #$10,D6 
      BNE       L0007 
      RTS 
L000A:MOVE.W    #0,-(A7)
      PEA       L003E(PC) 
      MOVE.W    #$3D,-(A7) 	;FOPEN
      TRAP      #1
      ADDQ.L    #8,A7 
      TST.W     D0
      BMI       L0001 
      MOVE.W    D0,D6 
      PEA       $1FD5E
      PEA       $BB8.L
      MOVE.W    D6,-(A7)
      MOVE.W    #$3F,-(A7) 	;FREAD
      TRAP      #1
      LEA       12(A7),A7 
      MOVE.W    D6,-(A7)
      MOVE.W    #$3E,-(A7) 	;FCLOSE 
      TRAP      #1
      ADDQ.L    #4,A7 
      MOVE.W    #0,-(A7)
      PEA       L003F(PC) 
      MOVE.W    #$3D,-(A7) 	;FOPEN
      TRAP      #1
      ADDQ.L    #8,A7 
      TST.W     D0
      BMI       L0001 
      MOVE.W    D0,D6 
      PEA       $26C10
      PEA       $5DC0.L 
      MOVE.W    D6,-(A7)
      MOVE.W    #$3F,-(A7) 	;FREAD
      TRAP      #1
      LEA       12(A7),A7 
      MOVE.W    D6,-(A7)
      MOVE.W    #$3E,-(A7) 	;FCLOSE 
      TRAP      #1
      ADDQ.L    #4,A7 
      RTS 
L000B:MOVEA.L   $456.L,A0 
L000C:TST.L     (A0)+ 
      BNE       L000C 
      LEA       $1FD5E,A1 
      LEA       -4(A0),A0 
      MOVE.L    A1,(A0) 
      MOVE.L    A0,L0011
      MOVE.B    #2,$274B2 
      MOVE.L    #L000D,$502.L 
L000D:RTS 
L000E:MOVEA.W   #$FA00,A0 
      LEA       L0012,A1
      MOVE.B    7(A0),(A1)+ 
      MOVE.B    19(A0),(A1)+
      MOVE.B    27(A0),(A1)+
      MOVE.B    33(A0),(A1)+
      MOVE.L    $120.L,(A1)+
      CLR.B     27(A0)
      BSET      #0,7(A0)
      BSET      #0,19(A0) 
      MOVE.L    #L001B,$120.L 
      MOVE.L    $70.L,L0018 
      MOVE.L    $118.L,L001A
      MOVE.L    #L0019,$118.L 
      MOVE.L    #L0013,$70.L
      MOVE.B    $FF820A.L,L0010 
      MOVE.B    #2,$FF820A.L
      MOVE.W    #$25,-(A7) 	;VSYNC
      TRAP      #$E 
      ADDQ.L    #2,A7 
      RTS 
L000F:MOVE      #$2700,SR 
      MOVEA.W   #$FA00,A0 
      LEA       L0012,A1
      MOVE.B    (A1)+,7(A0) 
      MOVE.B    (A1)+,19(A0)
      MOVE.B    (A1)+,27(A0)
      MOVE.B    (A1)+,33(A0)
      MOVE.L    (A1)+,$120.L
      MOVE.L    L001A,$118.L
      MOVE.L    L0018,$70.L 
      MOVE.B    #2,$FF820A.L
L0010:EQU       *-5 
      MOVE      #$2000,SR 
      MOVE.W    #0,$274B2 
      DC.B      'B',$B9 
L0011:DC.B      $FF,$FF,$FF,$FF 
      RTS 
L0012:DCB.W     4,0 
L0013:DC.B      'H',$E7,$FF,$E0,'4|',$FA,$00
      DC.B      'B*',$00,$1B,'R9' 
      DC.L      L0039 
      DC.B      $0C,'9' 
L0014:DC.B      $00,$03 
      DC.L      L0039 
      BNE       L0016 
      CLR.B     L0039 
      LEA       L0024,A0
      LEA       -2(A0),A1 
      MOVE.W    #$41,D0 	;#'A'
      MOVE.W    L0023,D7
L0015:MOVE.W    -(A1),-(A0) 
      DBF       D0,L0015
      MOVE.W    D7,L0022
L0016:MOVE.B    #4,33(A2) 
      MOVE.B    #8,27(A2) 
      CLR.W     L0021 
      ADDQ.B    #1,L003A
      CMPI.B    #6,L003A
      BNE       L0017 
      CLR.B     L003A 
      LEA       L001E,A0
      MOVE.W    (A0),D0 
      MOVE.W    2(A0),(A0)
      MOVE.W    D0,2(A0)
      CMPI.B    #$4C,L0036
      BNE       L001B 
      CMPI.B    #$52,L0037
      BNE       L001B 
      LEA       L001F,A0
      MOVE.W    (A0),D0 
      MOVE.W    2(A0),(A0)
      MOVE.W    D0,2(A0)
      LEA       L0020,A0
      MOVE.W    (A0),D0 
      MOVE.L    2(A0),(A0)
      MOVE.L    6(A0),4(A0) 
      MOVE.W    8(A0),6(A0) 
      MOVE.W    D0,8(A0)
      MOVEM.L   L001D,D0-D7 
      MOVEM.L   D0-D7,$FF8240.L 
L0017:MOVEM.L   (A7)+,A0-A2/D0-D7 
      JMP       0.L 
L0018:EQU       *-4 
L0019:MOVE      #$2500,SR 
      JMP       0.L 
L001A:EQU       *-4 
L001B:MOVEM.L   A0/D0,-(A7) 
      MOVE.W    L0021,D0
      ADDQ.W    #2,L0021
      MOVE.W    L0022(PC,D0.W),$FF825C.L
      CMP.W     #$82,D0 
      BNE.S     L001C 
      CLR.W     L0021 
L001C:MOVEM.L   (A7)+,A0/D0 
      BCLR      #0,$FFFA0F.L
      RTE 
L001D:DC.B      $00,$00,$02,$00,$03,$00,$04,$00 
      DC.B      $05,$00 
L001E:DC.B      $06,$00,$07,$00 
L001F:DC.B      $07,'w',$07,'r' 
L0020:DC.B      $03,$10,$04,'!',$05,'2',$06,'C' 
      DC.B      $07,'T',$07,'w',$07,'w' 
L0021:DC.B      $00,$00 
L0022:DC.B      $00,$02,$00,$03,$00,$04,$00,$05 
      DC.B      $00,$06,$00,$07,$00,$06,$00,$05 
      DC.B      $00,$04,$00,$03,$00,$02,$00,' ' 
      DC.B      $00,'0',$00,'@',$00,'P',$00,'`' 
      DC.B      $00,'p',$00,'`',$00,'P',$00,'@' 
      DC.B      $00,'0',$00,' ',$02,$00,$03,$00 
      DC.B      $04,$00,$05,$00,$06,$00,$07,$00 
      DC.B      $06,$00,$05,$00,$04,$00,$03,$00 
      DC.B      $02,$00,$00,'"',$00,'3',$00,'D' 
      DC.B      $00,'U',$00,'f',$00,'w',$00,'f' 
      DC.B      $00,'U',$00,'D',$00,'3',$00,'"' 
      DC.B      $02,' ',$03,'0',$04,'@',$05,'P' 
      DC.B      $06,'`',$07,'p',$06,'`',$05,'P' 
      DC.B      $04,'@',$03,'0',$02,' ',$02,$02 
      DC.B      $03,$03,$04,$04,$05,$05,$06,$06 
      DC.B      $07,$07,$06,$06,$05,$05,$04,$04 
      DC.B      $03,$03 
L0023:DC.B      $02,$02 
L0024:DC.B      $00,$00 
L0025:LEA       L0035,A2
L0026:MOVE.W    L0044,D7
      CMP.W     #3,D7 
      BEQ.S     L0028 
L0027:MOVE.W    #$25,-(A7) 	;VSYNC
      TRAP      #$E 
      ADDQ.L    #2,A7 
      DBF       D7,L0027
L0028:LEA       $78B08,A1 
      LEA       $78A68,A0 
      MOVEQ     #9,D0 
      MOVE.L    A2,L0042
      MOVE.L    A7,L0041
L0029:MOVE.W    D0,L0043
      MOVEM.L   0(A1),A2-A7/D0-D7 
      MOVEM.L   A2-A7/D0-D7,0(A0) 
      MOVEM.L   160(A1),A2-A7/D0-D7 
      MOVEM.L   A2-A7/D0-D7,160(A0) 
      MOVEM.L   320(A1),A2-A7/D0-D7 
      MOVEM.L   A2-A7/D0-D7,320(A0) 
      MOVEM.L   480(A1),A2-A7/D0-D7 
      MOVEM.L   A2-A7/D0-D7,480(A0) 
      MOVEM.L   640(A1),A2-A7/D0-D7 
      MOVEM.L   A2-A7/D0-D7,640(A0) 
      MOVEM.L   800(A1),A2-A7/D0-D7 
      MOVEM.L   A2-A7/D0-D7,800(A0) 
      MOVEM.L   960(A1),A2-A7/D0-D7 
      MOVEM.L   A2-A7/D0-D7,960(A0) 
      MOVEM.L   1120(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,1120(A0)
      MOVEM.L   1280(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,1280(A0)
      MOVEM.L   1440(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,1440(A0)
      MOVEM.L   1600(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,1600(A0)
      MOVEM.L   1760(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,1760(A0)
      MOVEM.L   1920(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,1920(A0)
      MOVEM.L   2080(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,2080(A0)
      MOVEM.L   2240(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,2240(A0)
      MOVEM.L   2400(A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,2400(A0)
      LEA       2560(A1),A1 
      LEA       2560(A0),A0 
      MOVE.W    L0043,D0
      DBF       D0,L0029
      MOVEA.L   L0045,A1
      MOVEQ     #0,D0 
      MOVE.B    L0034,D0
      MULU      #$A0,D0 
      ADDA.L    D0,A1 
      LEA       $7EDC8,A0 
      MOVEM.L   (A1),A2-A7/D0-D7
      MOVEM.L   A2-A7/D0-D7,(A0)
      LEA       160(A1),A1
      LEA       160(A0),A0
      MOVE.W    L0043,D0
      MOVEA.L   L0041,A7
      MOVEA.L   L0042,A2
      ADDQ.B    #1,L0034
      CMPI.B    #8,L0034
      BNE       L002A 
      CLR.B     L0034 
      PEA       L0033(PC) 
      MOVE.W    #9,-(A7) 	;CCONWS 
      TRAP      #1
      ADDQ.L    #6,A7 
      MOVE.L    A2,-(A7)
      MOVE.W    #9,-(A7) 	;CCONWS 
      TRAP      #1
      ADDQ.L    #6,A7 
      LEA       15(A2),A2 
      CMPA.L    #L0038,A2 
      BNE       L002A 
      LEA       L0035,A2
L002A:MOVEA.L   A2,A4 
      MOVE.W    #$FF,-(A7)
      MOVE.W    #6,-(A7) 	;CRAWIO 
      TRAP      #1
      ADDQ.L    #4,A7 
      MOVEA.L   A4,A2 
      CMP.W     #$20,D0 
      BEQ       L002B 
      CMP.W     #$30,D0 
      BEQ       L002C 
      CMP.W     #$31,D0 
      BEQ       L002D 
      CMP.W     #$32,D0 
      BEQ       L002E 
      CMP.W     #$33,D0 
      BEQ       L002F 
      CMP.W     #$37,D0 
      BEQ       L0030 
      CMP.W     #$38,D0 
      BEQ       L0031 
      CMP.W     #$39,D0 
      BEQ       L0032 
      BRA       L0026 
L002B:RTS 
L002C:MOVE.W    #3,L0044
      MOVE.W    #1,L0014
      MOVE.W    #0,L0039
      BRA       L0026 
L002D:MOVE.W    #0,L0044
      MOVE.W    #2,L0014
      MOVE.W    #0,L0039
      BRA       L0026 
L002E:MOVE.W    #1,L0044
      MOVE.W    #3,L0014
      MOVE.W    #0,L0039
      BRA       L0026 
L002F:MOVE.W    #2,L0044
      MOVE.W    #4,L0014
      MOVE.W    #0,L0039
      BRA       L0026 
L0030:MOVE.W    #2,$274B2 
      BRA       L0026 
L0031:MOVE.W    #7,$274B2 
      BRA       L0026 
L0032:MOVE.W    #8,$274B2 
      BRA       L0026 
L0033:DC.B      $1B,'c',$0E,$1B,'Y  ',$00 
      DC.B      $00,$00 
L0034:DC.B      $00,$00 
L0035:DC.B      '    TETRIS    ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      ' was cracked  ',$00
      DC.B      '      by      ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00 
      DC.B      '   ~~~~~~~~   ',$00
      DC.B      '   THE '
L0036:DC.B      'LO'
L0037:DC.B      'RD   ',$00
      DC.B      '   ~~~~~~~~   ',$00
      DC.B      '              ',$00
      DC.B      '     from     ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '*~~~~~~~~~~~~*',$00 
      DC.B      '|  S.T.C.S.  |',$00
      DC.B      '*~~~~~~~~~~~~*',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      ' Greetings to ',$00
      DC.B      ' the bests on ',$00
      DC.B      '   atari ST:  ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '    42 CREW   ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '      TEX     ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '    B.O.S.S.  ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '    TSUNOO    ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '      MMC     ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '      TNT     ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '      HOB     ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '    Others    ',$00
      DC.B      ' greetings to:',$00
      DC.B      '              ',$00
      DC.B      'Cat',$27,'s club    ',$00
      DC.B      'The contact   ',$00 
      DC.B      'Rocket Raccoon',$00
      DC.B      'Kouk, Falcon-X',$00
      DC.B      'A-ha, CSS, BXC',$00
      DC.B      'The Cobra, AOD',$00
      DC.B      'Wourdalak     ',$00
      DC.B      'Cr. German Man',$00
      DC.B      '              ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '  THIS GAME   ',$00
      DC.B      '  IS FOR THE  ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      ' BLADE RUNNERS',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '   SET THE    ',$00
      DC.B      ' SPEED OF THIS',$00 
      DC.B      '   VERTICAL   ',$00
      DC.B      '              ',$00
      DC.B      '  C  C  C  C  ',$00
      DC.B      '  H  H  H  H  ',$00
      DC.B      '  I  I  I  I  ',$00
      DC.B      '  N  N  N  N  ',$00
      DC.B      '  E  E  E  E  ',$00
      DC.B      '  S  S  S  S  ',$00 
      DC.B      '  E  E  E  E  ',$00
      DC.B      '              ',$00
      DC.B      '  S  S  S  S  ',$00
      DC.B      '  C  C  C  C  ',$00
      DC.B      '  R  R  R  R  ',$00
      DC.B      '  O  O  O  O  ',$00
      DC.B      '  L  L  L  L  ',$00
      DC.B      '  L  L  L  L  ',$00 
      DC.B      '  I  I  I  I  ',$00
      DC.B      '  N  N  N  N  ',$00
      DC.B      '  G  G  G  G  ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      ' BY PRESSING  ',$00
      DC.B      'THE FOLLOWING ',$00 
      DC.B      '    KEYS:     ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '  0 -> FASTEST',$00
      DC.B      '  1 -> FAST   ',$00
      DC.B      '  2 -> NORMAL ',$00
      DC.B      '  3 -> SLOW   ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      'or if you want',$00
      DC.B      ' by pressing  ',$00
      DC.B      'the following ',$00
      DC.B      '     keys:    ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '0->TURBO      ',$00
      DC.B      '1->FASTER THAN',$00
      DC.B      'GOLDRUNNER 1  ',$00
      DC.B      '2->SAME AS    ',$00
      DC.B      'GOLDRUNNER 1  ',$00 
      DC.B      '3->DROOPY',$27,'s   ',$00
      DC.B      '   SCROLLING  ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      'This vertical ',$00
      DC.B      'pixel by pixel',$00 
      DC.B      '  scrolling   ',$00
      DC.B      'will be in the',$00
      DC.B      'future removed',$00
      DC.B      '    by an     ',$00
      DC.B      '  horizontal  ',$00
      DC.B      '  scrolling.  ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '   Bored by   ',$00
      DC.B      'this wonderful',$00
      DC.B      '    music?    ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      ' Change it by ',$00
      DC.B      ' pressing the ',$00
      DC.B      'following keys',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      ' 7 or 8 or 9  ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00 
      DC.B      'The Members of',$00
      DC.B      'S.T.C.S. are: ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '  ACTARUS     ',$00
      DC.B      '              ',$00
      DC.B      '  BILLY OCTET ',$00
      DC.B      '              ',$00 
      DC.B      '  THE LORD    ',$00
      DC.B      '              ',$00
      DC.B      '  SUN STRIKE  ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00 
      DC.B      '\            /',$00
      DC.B      ' \          / ',$00
      DC.B      '  \        /  ',$00
      DC.B      '   \      /   ',$00
      DC.B      '    \    /    ',$00
      DC.B      '     \  /     ',$00
      DC.B      '      \/      ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '    /\oo/\    ',$00
      DC.B      '              ',$00
      DC.B      '______________',$00
      DC.B      '              ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00
      DC.B      '              ',$00 
      DC.B      '              ',$00
L0038:DC.B      '              ',$00 
L0039:DC.B      $00,$00 
L003A:DC.B      $00,$00 
L003B:DC.B      $1A,$12 
L003C:DC.B      $14,$08 
L003D:DC.B      'VOL1',$00
L003E:DC.B      'VOL3',$00
L003F:DC.B      'VOL4',$00
L0040:DC.B      'VOL2',$00
L0041:DCB.W     2,0 
L0042:DCB.W     2,0 
L0043:DC.B      $00,$00 
L0044:DC.B      $00,$01 
L0045:DCB.W     2,0 
L0046:DCB.W     2,0 
L0047:DC.B      $00,$00 
L0048:DCB.W     2,0 
L0049:DCB.W     2,0 
L004A:DCB.W     16,0
L004B:DCB.W     616,0 
L004C:DCB.W     2,0 
ZUEND: END
